
Topology3D to Netkit

    documents -> all project documentation
    netkit-deploy -> read first the section 4 of the report to know how to test it, but the essential file is netkit-deployer.py

The documents folder is organized in subfolders

    report -> main report document
    presentation -> presentation

Instructions

    Copy the netkit-deploy folder to /home/user/workspace
    Go to /home/user/workspace/Dreamer-Experiment-Handler/Control and open the file tbcontroller.js
    Change these lines and save (see section 4 of the report)
    Then start Topology3D, select your oshi topology, then �deployment� and then write �netkit�
    When is finished go to where it indicates and see all the files
    You need to give permissions to all users, because the folders where made by the root user, so open the terminal and do this(see section 4 of the report)
    Now go to your lab directory and start the lab with �lstart�

